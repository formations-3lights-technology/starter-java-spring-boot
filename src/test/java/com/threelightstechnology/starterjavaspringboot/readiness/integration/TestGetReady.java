package com.threelightstechnology.starterjavaspringboot.readiness.integration;

import com.threelightstechnology.starterjavaspringboot.readiness.domain.ReadyGlobalStatus;
import com.threelightstechnology.starterjavaspringboot.readiness.usecases.ReadyChecker;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestGetReady {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private ReadyChecker readyChecker;

    @Nested
    @DisplayName("Returns 200")
    class Returns200 {
        @Test
        void tout_est_ok() {
            // GIVEN
            given(readyChecker.handle()).willReturn(new ReadyGlobalStatus());

            // WHEN
            var response = testRestTemplate.exchange(
                    "/v1/ready",
                    HttpMethod.GET,
                    null,
                    String.class
            );

            // THEN
            verify(readyChecker).handle();
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
            assertThat(Objects.requireNonNull(response.getBody())).hasToString(
                    "{" +
                            "\"apiVersion\":\"1.0.0\"," +
                            "\"apiStatus\":\"UP\"" +
                            "}"
            );
        }
    }
}

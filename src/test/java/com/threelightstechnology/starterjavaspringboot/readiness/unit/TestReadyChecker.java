package com.threelightstechnology.starterjavaspringboot.readiness.unit;

import com.threelightstechnology.starterjavaspringboot.readiness.usecases.ReadyChecker;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class TestReadyChecker {
    @Test
    void tout_est_ok() {
        // WHEN
        var result = new ReadyChecker().handle();

        // THEN
        assertThat(result.isUp()).isTrue();
    }
}

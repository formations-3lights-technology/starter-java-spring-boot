package com.threelightstechnology.starterjavaspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarterJavaSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarterJavaSpringBootApplication.class, args);
    }

}

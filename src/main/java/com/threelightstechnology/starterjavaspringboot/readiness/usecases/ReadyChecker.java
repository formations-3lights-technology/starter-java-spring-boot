package com.threelightstechnology.starterjavaspringboot.readiness.usecases;

import com.threelightstechnology.starterjavaspringboot.readiness.domain.ReadyGlobalStatus;
import org.springframework.stereotype.Service;

@Service
public class ReadyChecker {
    public ReadyGlobalStatus handle() {
        return new ReadyGlobalStatus();
    }
}

package com.threelightstechnology.starterjavaspringboot.readiness.domain;

public enum ApiStatus {
    UP,
    DOWN
}

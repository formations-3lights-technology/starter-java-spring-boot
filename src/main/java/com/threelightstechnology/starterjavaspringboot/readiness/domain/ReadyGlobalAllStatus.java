package com.threelightstechnology.starterjavaspringboot.readiness.domain;

public class ReadyGlobalAllStatus {
    private final String apiVersion;
    private final ApiStatus apiStatus;

    public ReadyGlobalAllStatus(String apiVersion, ApiStatus apiStatus) {
        this.apiVersion = apiVersion;
        this.apiStatus = apiStatus;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public ApiStatus getApiStatus() {
        return apiStatus;
    }
}

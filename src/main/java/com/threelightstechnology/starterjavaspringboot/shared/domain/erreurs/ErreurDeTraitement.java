package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

public class ErreurDeTraitement extends RuntimeException {
  public ErreurDeTraitement(String message) {
    super(message);
  }
}

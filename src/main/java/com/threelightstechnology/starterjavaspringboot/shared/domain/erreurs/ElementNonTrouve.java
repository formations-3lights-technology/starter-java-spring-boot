package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

public class ElementNonTrouve extends RuntimeException {
  public ElementNonTrouve(String message) {
    super(message);
  }
}

package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

public class ErreurDeValidation extends RuntimeException {
  public ErreurDeValidation(String message) {
    super(message);
  }
}

package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

public class EchecDependance extends RuntimeException {
  public EchecDependance(
          int statusCode,
          String statusMessage,
          String message,
          Object body
  ) {
    super(message);
  }
}

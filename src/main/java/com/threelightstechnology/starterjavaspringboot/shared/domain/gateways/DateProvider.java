package com.threelightstechnology.starterjavaspringboot.shared.domain.gateways;

import java.time.LocalDateTime;

public interface DateProvider {
    LocalDateTime now();
}

package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

import static java.text.MessageFormat.*;

public class OptimisticConcurrency extends ElementEnConflit {
  public OptimisticConcurrency(String nom, int version) {
    super(format("{0}: the version {1} is not the current version", nom, version));
  }
}

package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

import static java.text.MessageFormat.*;

public class LimiteAppel extends RuntimeException {
  public LimiteAppel(int appelMax, int intervalEnMilliseconde) {
    super(format("Rate limit: {0} per {1}ms", appelMax, intervalEnMilliseconde));
  }
}

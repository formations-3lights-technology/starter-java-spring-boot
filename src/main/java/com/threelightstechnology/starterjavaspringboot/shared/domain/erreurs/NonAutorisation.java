package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

public class NonAutorisation extends RuntimeException {
  public NonAutorisation(String message) {
    super(message);
  }
}

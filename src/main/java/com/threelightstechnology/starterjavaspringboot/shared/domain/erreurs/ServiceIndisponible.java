package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

public class ServiceIndisponible extends RuntimeException {
  public ServiceIndisponible() {
    super("Service unavailable");
  }
}

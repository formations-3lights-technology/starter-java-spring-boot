package com.threelightstechnology.starterjavaspringboot.shared.domain.erreurs;

public class ActionNonAutorisee extends RuntimeException {
  public ActionNonAutorisee() {
    super("Current user does not have good rights");
  }
}

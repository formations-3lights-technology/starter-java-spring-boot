package com.threelightstechnology.starterjavaspringboot.shared.infrastructure.gateways;

import com.threelightstechnology.starterjavaspringboot.shared.domain.gateways.DateProvider;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class RealDateProvider implements DateProvider {
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}

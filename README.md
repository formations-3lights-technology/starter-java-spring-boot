# Stater Java + Spring Boot

## JENV

Le starter a un fichier `.java-version` permettant d'utiliser `jenv`.

Si `jenv` ne s'est pas mis automatiquement sur la bonne version, faire `jenv add 17.0`.

## Makefile

Le starter a un fichier `Makefile` permettant d'utiliser la commande `make`.

Pour connaitre les commandes disponibles, taper `make`. Il faut installer `make` au préalable.

NB : Les commandes les plus importantes peuvent être lancées avec `gradle`.

## Lancer le projet (via make)

1. Lancer le projet : `make start`
2. Vérifier que le projet est bien démarré : `http://localhost:8080/v1/ready` doit afficher : `"apiStatus":"UP"`
3. S'il y a besoin d'une base de données, un Docker compose est présent avec un service Postgres configuré.
   - Pour le démarrer : `make start-dep`
     <br> ⚠️ il n'y a pas de volumes pour persister les données
   - Les identifiants utilisés pour configurer Postgres sont dans le fichier `docker-compose.yaml`

## Autres commandes (via make)

- Pour lancer les tests : `make test`

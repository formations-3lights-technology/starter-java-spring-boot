SHELL := /bin/bash
.SHELLFLAGS = -e -c
.SILENT:
.ONESHELL:

.EXPORT_ALL_VARIABLES:
ROOT_DIR := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))
GRADLE_CMD := gradle -b "$(ROOT_DIR)/build.gradle"

.DEFAULT_GOAL: help

.PHONY: help
help:
	@echo "Please use 'make <target>' where <target> is one of"
	@grep -E '^\.PHONY: [a-zA-Z_-]+ .*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = "(: |##)"}; {printf "\033[36m%-30s\033[0m %s\n", $$2, $$3}'

.PHONY: start-dep ## ▶ Launch dependencies
start-dep:
	docker compose down
	sleep 2
	docker compose up -d --remove-orphans

.PHONY: stop-dep ## ⏸ Stop dependencies
stop-dep:
	docker compose down

.PHONY: start ## 🎮 Start locally
start:
	$(GRADLE_CMD) bootRun

.PHONY: test ## ✅ Launch test
test:
	$(GRADLE_CMD) check -i

.PHONY: clean ## ✅ Clean project
clean:
	rm -rf .gradle
	rm -rf build
	$(GRADLE_CMD) clean

.PHONY: docker-clean ## 🧹 Docker prune
docker-clean:
	docker system prune --all

.PHONY: docker-clean-volume ## 🛑🛑🛑 🧹 Docker volumes prune 🛑🛑🛑
docker-clean-volume:
	docker system prune --all --volumes
